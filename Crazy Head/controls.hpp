//
//  controls.hpp
//  Crazy Head
//
//  Created by Redwan Ahsan on 4/26/18.
//  Copyright © 2018 Redwan Ahsan. All rights reserved.
//

#ifndef controls_hpp
#define controls_hpp
#include <glm/glm.hpp>

#include <stdio.h>

void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();

#endif /* controls_hpp */
